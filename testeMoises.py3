
# BIBLIOTECA DO CAPIROTOO. 
# SO ESSA FUNCIONA COMO O C FUNCIONA, ENTÂO NAO MUDE
from socketIO_client_nexus import SocketIO, LoggingNamespace

def relay(self):
    print ('relay:', self)
    # AQUI A PLACA LIGA OS RELE
    # COLOCAR A REESPOSTA PARA O SERVIDOR NODE AQUI

# AQUI CONECTA COM O SOCKET
io = SocketIO('187.191.98.227', 3020)


# ISSO AQUI TU DEFINE UMA RESPOSTA CASO TU RECEBA O IDENTIFICADOR relay
# ELE EXECUTA A FUNCAO relay "def relay(self):..."
io.on('relay',relay) 


# ISSO AQUI ENVIA O JOIN
data = {
        "id" : "5c:cf:7f:b5:da:6a",
    }
io.emit('join',data)

# ISSO AQUI DEIXA CONECTADO
io.wait()


# A PLACA SE COMPORTA UM POUCO DIFERENTE, 
# NÂO VAI DAR TEMPO DE FAZER TUDO, 
# MAS COM ISSO TU JA PODE SIMULAR UMA COMUNICACAO